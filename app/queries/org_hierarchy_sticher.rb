class OrgHierarchySticher
    attr_accessor :org_level4s_seen, :org_level5s_seen, :org_level6s_seen

    def initialize(orgs, level4_orgs, level5_orgs, level6_orgs)
        @orgs = orgs
        @org_level4_hash = level4_orgs
        @org_level5_hash = level5_orgs
        @org_level6_hash = level6_orgs
        @org_level4s_seen = {}
        @org_level5s_seen = {}
        @org_level6s_seen = {}
    end

    def set_orgs_tree_structure
        orgs.each do |org|
            if org.organization_level6_id
                set_org_levels(org, org.organization_level6_id, org_level6s_seen, org_level6_hash)
            elsif org.organization_level5_id
                set_org_levels(org, org.organization_level5_id, org_level5s_seen, org_level5_hash)
            elsif org.organization_level4_id
                set_org_levels(org, org.organization_level4_id, org_level4s_seen, org_level4_hash)
            end
        end
    end

    def set_parent_child_orgs_relationship
        orgs.each do |org|
            org_level4_id = org.organization_level4_id
            org_level5_id = org.organization_level5_id
            org_level6_id = org.organization_level6_id

            org_level4 = org_level4_hash[org_level4_id]
            org_level5 = org_level4_hash[org_level5_id]
            org_level6 = org_level4_hash[org_level6_id]

            next unless org_level4
            next unless org_level5
            next unless org_level6

            org_level5.parent = org_level4
            org_level5.add_to_child_orgs(org_level6)
            org_level6.parent = org_level5
            org_level4.add_to_child_orgs(org_level5)

            org_level4s_seen[org_level4.id] = org_level4
        end
    end

    private

    attr_reader :orgs, :org_level4_hash, :org_level5_hash, :org_level6_hash

    def set_org_levels(org, org_level_id, org_level_seen, org_level_hash)
        org_level = org_level_hash[org_level_id]
        if org_level.name == org.name
            org_level.the_org = org
        else
            org_level.add_to_child_orgs(org)
        end
        org_level_seen[org_level.id] = org_level
    end
end