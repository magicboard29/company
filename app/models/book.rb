class Book < ApplicationRecord

    belongs_to :author
    has_one :organization, through: :author
    has_one :organization_level4, through: :organization
    has_one :organization_level5, through: :organization
    has_one :organization_level6, through: :organization

    scope :by_statuses, -> { where(status: %w[pending published progress]) }

    scope :by_organization, lambda { |organization|
        joins(:organization).where(organizations: { id: organization })
    }
    scope :by_organization_level4, lambda { |organization_level4|
        joins(:organization_level4).where(organization_level4s: { id: organization_level4 })
    }
    scope :by_organization_level5, lambda { |organization_level5|
        joins(:organization_level5).where(organization_level5s: { id: organization_level5 })
    }
    scope :by_organization_level6, lambda { |organization_level6|
        joins(:organization_level6).where(organization_level6s: { id: organization_level6 })
    }
end
