class Author < ApplicationRecord
    belongs_to :organization

    has_many :books
    has_one :organization_level4, through: :organization
    has_one :organization_level5, through: :organization
    has_one :organization_level6, through: :organization
end
