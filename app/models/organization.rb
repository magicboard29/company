class Organization < ApplicationRecord
    belongs_to :organization_level4
    belongs_to :organization_level5
    belongs_to :organization_level6
end
