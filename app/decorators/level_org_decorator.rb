class LevelOrgDecorator < ::SimpleDelegator
    attr_accessor :books, :child_orgs, :the_org, :parent, :user

    def initialize(org)
        super
        @org = org
        @child_orgs = Set.new
        @books = []
    end

    def add_to_child_orgs(value)
        child_orgs << value
    end

    def total_books
        length = books.length
        length
    end

    def count_by_status
        books.group_by(&:status).transform_values(&:count)
    end

    def count_by_status_by_gender_data
        books.group_by { |book| book.author.gender }
            .to_h
            .transform_values { |books| books.group_by(&:status).transform_values(&:count) }
    end

    def count_by_grade_by_status_by_gender
        books.group_by { |book| book.author.grade }
            .transform_values { |books| books.group_by { |book| book.author.gender}
            .transform_values { |books| books.group_by(&:status).transform_values(&:count) } }
    end
end