
class AnalyticsController < ApplicationController

  before_action :fetch_org_hierarchy_sticher,
                only: %i[index]
  
  def index; end

  private

  def set_orgs_tree
    orgs = map_books_to_orgs
    org_level4_hash = map_org_hash(@organization_level4s)
    org_level5_hash = map_org_hash(@organization_level5s)
    org_level6_hash = map_org_hash(@organization_level6s)

    stich_books_level4_orgs_level4(org_level4_hash)
    stich_books_level5_orgs_level5(org_level5_hash)
    stich_books_level6_orgs_level6(org_level6_hash)
    
    @org_hierarchy_sticher = OrgHierarchySticher.new(orgs, org_level4_hash, org_level5_hash, org_level6_hash)

    @org_hierarchy_sticher.set_orgs_tree_structure

    @org_hierarchy_sticher.set_parent_child_orgs_relationship

    org_hierarchy_sticher =  $redis.get("org_hierarchy_sticher")
  end

  # run a rake task every hour / 30 mins to warm the cache
  # org_hierarchy_sticher = @org_hierarchy_sticher.to_json
  # $redis.set("org_hierarchy_sticher", org_hierarchy_sticher)

  def fetch_org_hierarchy_sticher
    org_hierarchy_sticher = $redis.get("org_hierarchy_sticher")
    if org_hierarchy_sticher.nil?
      set_organizations
      set_organization_level4s
      set_organization_level5s
      set_organization_level6s
      set_books_org_levels
      set_orgs_tree
      org_hierarchy_sticher = @org_hierarchy_sticher.to_yaml
      $redis.set("org_hierarchy_sticher", org_hierarchy_sticher)
    end
    @org_hierarchy_sticher = YAML.load(org_hierarchy_sticher)
  end

  def map_org_hash(org_level)
      org_level_hash = {}
      org_level.each { |org| org_level_hash[org.id] = LevelOrgDecorator.new(org) }
      org_level_hash
  end

  def map_books_to_orgs
    @books.map do |book|
      org = book.organization
      org
    end
  end 

  def set_organizations
    @org_ids = Organization.all.pluck(:id) # example that this is a scope
    @organizations = Organization.where(id: @org_ids).order(:name)
  end

  def set_organization_level4s
    @org_level4_ids = @organizations.pluck(:id).uniq
    @organization_level4s = OrganizationLevel4.where(id: @org_level4_ids).order(:name)
  end

  def set_organization_level5s
    @org_level5_ids = @organizations.pluck(:id).uniq
    @organization_level5s = OrganizationLevel5.where(id: @org_level5_ids).order(:name)
  end

  def set_organization_level6s
    @org_level6_ids = @organizations.pluck(:id).uniq
    @organization_level6s = OrganizationLevel6.where(id: @org_level6_ids).order(:name)
  end

  def set_books_org_levels
    @books = Book.includes(:author, :organization).by_statuses
                 .by_organization(@org_ids)

    @books_level4s = Book.includes(:author, :organization_level4).by_statuses
                         .by_organization_level4(@org_level4_ids)

    @books_level5s = Book.includes(:author, :organization_level5).by_statuses
                         .by_organization_level5(@org_level5_ids)

    @books_level6s = Book.includes(:author, :organization_level6).by_statuses
                         .by_organization_level6(@org_level6_ids)
  end

  def stich_books_level4_orgs_level4(org_level4_hash)
    @books_level4s.map do |book|
      org_id = book.organization_level4.id
      org_level4_hash[org_id].books << book
    end
  end

  def stich_books_level5_orgs_level5(org_level5_hash)
    @books_level5s.map do |book|
      org_id = book.organization_level5.id
      org_level5_hash[org_id].books << book
    end
  end

  def stich_books_level6_orgs_level6(org_level6_hash)
    @books_level6s.map do |book|
      org_id = book.organization_level6.id
      org_level6_hash[org_id].books << book
    end
  end
end
