# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# 500.times do |i|    
#     level4_name = Faker::Company.name
#     OrganizationLevel4.create({ name: level4_name})
# end

# 1000.times do |i|
#     level5_name = Faker::Company.name
#     OrganizationLevel5.create({ name: level5_name})
# end

# 3000.times do |i|
#     level6_name = Faker::Company.name
#     OrganizationLevel6.create({ name: level6_name})
# end

# 5000.times do |i|
#     org4s = OrganizationLevel4.all.pluck(:id)
#     org5s = OrganizationLevel5.all.pluck(:id)
#     org6s = OrganizationLevel6.all.pluck(:id)
#     org_name = Faker::Company.name
#     org = Organization.create({ name: org_name, organization_level4_id: org4s.sample, organization_level5_id: org5s.sample, organization_level6_id: org6s.sample})
# end

# orgs = Organization.all.pluck(:id)

# 6000.times do |i|
#     author_name = Faker::Name.name
#     author = Author.create({ name: author_name, gender: ["male", "female"].sample, grade: ["g1", "g2", "g3", "g5", "g6"].sample, organization_id: orgs.sample })

#     book_name = Faker::Books::CultureSeries.book
#     Book.create({ title: book_name, author_id: author.id })
# end

# books = Book.all

# books.each do |book|
#     statuses = ['pending', 'published', 'progress']

#     book.update(status: statuses.sample)
# end