class CreateAuthors < ActiveRecord::Migration[6.0]
  def change
    create_table :authors do |t|
      t.string :name
      t.bigint :organization_id
      t.string :gender
      t.string :grade

      t.timestamps
    end
  end
end
