class CreateOrganizationLevel6s < ActiveRecord::Migration[6.0]
  def change
    create_table :organization_level6s do |t|
      t.string :name

      t.timestamps
    end
  end
end
