class CreateOrganizationLevel5s < ActiveRecord::Migration[6.0]
  def change
    create_table :organization_level5s do |t|
      t.string :name

      t.timestamps
    end
  end
end
