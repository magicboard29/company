class CreateOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :organizations do |t|
      t.string :name
      t.bigint :organization_level4_id
      t.bigint :organization_level5_id
      t.bigint :organization_level6_id

      t.timestamps
    end
  end
end
